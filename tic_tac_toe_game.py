# InApp Week - 2 Assignment
# Author      : Aanandhi V B
# Date        : November 12, 2020
# Description : A program to implement the Tic-Tac-Toe Game
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-2-assignment/-/blob/master/tic_tac_toe_game.py

import numpy as np
import random
print(
'''
Press the cell number where you want to keep your symbol

  1  |  2  |  3
-----|-----|-----
  4  |  5  |  6
-----|-----|-----
  7  |  8  |  9
''')

def print_board(game_board):

    print("{}|{}|{}".format(game_board[0][0], game_board[0][1], game_board[0][2]))
    print("{}|{}|{}".format(game_board[1][0], game_board[1][1], game_board[1][2]))
    print("{}|{}|{}".format(game_board[2][0], game_board[2][1], game_board[2][2]))

try:
    user_symbol = int(input("Choose your symbol - X (1) or O (0): "))
    computer_symbol = 1 if user_symbol == 0 else 0
    winner, user_choice, computer_choice, rounds = "", 0, 0, 1
    combinations = {1: "00", 2: "01", 3: "02", 4: "10", 5: "11", 6: "12", 7: "20", 8: "21", 9: "22", "game_history":{}}
    while rounds < 11:
        print("----- ROUND {} ----".format(rounds))
        game_board = np.full((3, 3), 7, dtype=int)
        cell_numbers = list(range(1, 10))
        user_moves, computer_moves = [], []
        turns, flag = 1, 0
        while turns < 10:
                print_board(game_board)
                user_choice = int(input("\nEnter cell number: "))
                cell_numbers = list(filter(lambda x: x != user_choice, cell_numbers))
                if len(cell_numbers) > 0: computer_choice = random.choice(cell_numbers)
                else:
                    winner = "None"
                    break
                cell_numbers = list(filter(lambda x: x != computer_choice, cell_numbers))
                print("Choose from these cells only: ", cell_numbers)
                turns += 1
                user_moves.append(user_choice)
                computer_moves.append(computer_choice)
                game_board[int(combinations[user_choice][0])][int(combinations[user_choice][1])] = user_symbol
                game_board[int(combinations[computer_choice][0])][int(combinations[computer_choice][1])] = computer_symbol
                for i in range(0, 3):
                        if np.all(game_board[i] == user_symbol) or np.all(game_board[:, i] == user_symbol) or np.all(np.diag(game_board) == user_symbol) or np.all(np.diag(np.fliplr(game_board) == user_symbol)):
                                winner = "User"
                                flag = 1
                                break
                        elif np.all(game_board[i] == computer_symbol) or np.all(game_board[:, i] == computer_symbol) or np.all(np.diag(game_board) == computer_symbol) or np.all(np.diag(np.fliplr(game_board) == computer_symbol)):
                                flag = 1
                                winner = "Computer"
                                break
                if flag == 1:
                        break
        combinations["game_history"][rounds] = {"User": "Choice: {}, Moves: {}".format(user_symbol, user_moves), "Computer": "Choice: {}, Moves: {}".format(computer_symbol, computer_moves), "Winner": winner}
        rounds += 1

    round_info = int(input("\nEnter the round between 0-10 for which you need the information: "))
    if round_info < 11:
                    print("User = {}".format(combinations["game_history"][round_info]["User"]))
                    print("Computer = {}".format(combinations["game_history"][round_info]["Computer"]))
                    print("{} won round {}".format(combinations["game_history"][round_info]["Winner"], round_info))
                    print("\nThank you for playing!")
    else:
                    print("Invalid Round!")

except Exception as e:
        print("Error is: {}!".format(e))



